@ProductAPI
Feature: Search for the product

  Scenario Outline: Testing Products is exist
    When user calls endpoint for product "<product>"
    Then user sees the results displayed for "<product>"
    Examples:
      | product |
      | cola    |
      | apple   |
      | pasta   |

  Scenario Outline: Testing Products is not exist
    When user calls endpoint for product "<product>"
    Then user does not see the results
    Examples:
      | product   |
      | car       |
      | chocolate |
      | tomato    |
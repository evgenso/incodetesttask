package starter.utils.products;

import net.serenitybdd.rest.SerenityRest;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class ApiProductActions {

    final private String baseUrl = "https://waarkoop-server.herokuapp.com/api/v1/search/demo/";

    public void getRequest(String endpoint) {
        SerenityRest.given().get(baseUrl + endpoint).then().log().all();;
    }

    public void assertProductIsExistInTitle(String product) {
        restAssuredThat(response -> response.body("title", hasItems(containsString(product))));
    }

    public void assertProductNotFound() {
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));
    }
}

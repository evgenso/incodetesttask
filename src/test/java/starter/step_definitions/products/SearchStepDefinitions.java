package starter.step_definitions.products;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import starter.utils.products.ApiProductActions;


public class SearchStepDefinitions {

    final private ApiProductActions apiProductActions = new ApiProductActions();

    @When("user calls endpoint for product {string}")
    public void userCallsEndpointForProduct(String product) {
        apiProductActions.getRequest(product);
    }

    @Then("user sees the results displayed for {string}")
    public void userSeesTheResultsDisplayedForProduct(String product) {
        apiProductActions.assertProductIsExistInTitle(product);
    }

    @Then("user does not see the results")
    public void userDoesNotSeeTheResults() {
        apiProductActions.assertProductNotFound();
    }
}

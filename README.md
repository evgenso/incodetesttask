# 1.  **Clone the Repository:**
### Choose "Get from Version Control": In the main menu of IntelliJ IDEA, go to "File" > "New" > "Project from Version Control..."
### Paste the Repository URL: In the "Clone Repository" dialog that appears, paste the [URL](https://gitlab.com/evgenso/incodetesttask.git). Choose a directory on your computer where you want to copy the project.
### Make sure that "Git" is selected in the "Version control" field. Click the "Clone" button to start the repository cloning process.

# 2. **Project Configuration:**
### Make sure that the synchronization with Maven has taken place. Ensure your pom.xml file is correctly configured with dependencies and plugin settings for Serenity and Cucumber. Verify that all required dependencies are accurately specified.

# 3. **Write Tests:**
### The repository already contains feature files and step definitions. Enhance existing scenarios or create new feature files for additional tests.

# 4. **Run Tests:**
### Execute tests with this command:
`-mvn clean verify -Dcucumber.filter.tags="{tag}"`
### If you changing/adding a tag, you need to change one in .gitlab-ci.yml file.

# 6. **View Results:**
### Check test execution outcomes and Serenity reports in the target/site/serenity directory. Or you also can check a [gitLab result page](https://evgenso.gitlab.io/incodetesttask/) for actual run, or any another run in [artifacts page](https://gitlab.com/evgenso/incodetesttask/-/artifacts)(for last 24h).

##### _**Lastly, emphasize adhering to coding best practices and conventions for all team members involved in writing tests. Consistent and clean code enhances maintainability and teamwork. For any inquiries or issues, don't hesitate to seek assistance. Happy testing and coding!**_ ###

-------------------------------------------------------------

I separated the test and HTML logic, changed the name in the steps from “he” to “user” (I also made a version of the project with the screenplay pattern, but I didn’t dare to publish it, besides, it’s not mentioned in the T/A, but if it is necessary i can share it)
Removed repetitive steps, moved the parameter to a table separately from the function, got rid of repetitions, achieved easier scaling of the initially proposed tests.
Changed string content check to correct(in test). Made the tree of folders and files correct.
Added tags to the feature file, removed unused settings from the pom.xml file.
Removed unused files from the element tree, edited TestRunner.java and serenity.conf to correctly run tests and generate reports.
Added .gitlab-ci.yml file to edit pipeline settings. Changed README.md according to T/A
